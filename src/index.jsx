import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'

import Crud from './crud/app'


const App = () => {

  return(
    <Router>
      <div className="crudMenu">
        <Link to='/orders'>Orders</Link>
        <Link to='/customers'>Customers</Link>
      </div>
      <Route path='/orders'
        render={() => {
          return(
            <Crud
              title="Order Items"
              canCreate={false}
              canEdit={false}
              canDelete={false}
              items={[
                {
                  id: 'order-1',
                  title: 'item 1',
                  text: 'Some info',
                  selected: true
                },
                {
                  id: 'order-2',
                  title: 'item 2',
                  text: 'Some info 2',
                  selected: false
                },
                {
                  id: 'order-3',
                  title: 'item 3',
                  text: 'Some info 3',
                  selected: false
                },
                {
                  id: 'order-4',
                  title: 'item 4',
                  text: 'Some info 4',
                  selected: false
                }
              ]}
            />
          )
        }}
      />
      <Route path='/customers'
        render={() => {
          return(
            <Crud
              title="customers Items"
              canCreate={true}
              canEdit={true}
              canDelete={true}
              items={[
                {
                  id: 'customers-1',
                  title: 'item 1',
                  text: 'Some info',
                  selected: true
                },
                {
                  id: 'customers-2',
                  title: 'item 2',
                  text: 'Some info 2',
                  selected: false
                },
                {
                  id: 'customers-3',
                  title: 'item 3',
                  text: 'Some info 3',
                  selected: false
                },
                {
                  id: 'customers-4',
                  title: 'item 4',
                  text: 'Some info 4',
                  selected: false
                }
              ]}
            />
          )
        }}
      />
    </Router>
  )

}


ReactDOM.render(<App />, document.getElementById('root'))