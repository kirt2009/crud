import React,{Component} from 'react'

import CrudHeader from './header'
import CrudItemList from './itemList'
import CrudBottom from './bottom'

import './style.css'

export default class Crud extends Component {

  // id для  новых элементов
  id = 100
  // id для  новых элементов

  state = {
    headerTitle: this.props.title,
    canCreate: this.props.canCreate,
    canEdit: this.props.canEdit,
    canDelete: this.props.canDelete,
    items: this.props.items
  }

  selectItem = (id) => {
    this.setState(({items}) => {
      const newItems = items.map((item) => {
        const newItem = {...item}
        if(item.id === id){
          newItem.selected = !item.selected
        }
        return newItem
      })
      return {items:newItems}
    })
  }

  deleteSelectItems = () => {
    this.setState(({items}) => {
      let newItemsList = []
      for(let i = 0; i < items.length; i++){
        if(!items[i]['selected']){
          newItemsList.push({...items[i]})
        }
      }
      return {items:newItemsList}
    })
  }

  selectUnselect = () => {
    this.setState(({items}) => {
      let newItemsList = []
      let countSelected = 0
      for(let i = 0; i < items.length; i++){
        if(items[i]['selected']){
          countSelected++
        }
      }
      if(countSelected === items.length){
        for(let i = 0; i < items.length; i++){
          newItemsList.push({...items[i],selected:false})
        }
      }else{
        for(let i = 0; i < items.length; i++){
          newItemsList.push({...items[i],selected:true})
        }
      }
      return {items:newItemsList}
    })
  }

  setText = (id,text) => {
    this.setState(({items}) => {
      let newItemsList = items.map((item) => {
        if(item.id === id){
          return {...item,text:text}
        }else{
          return {...item}
        }
      })
      return{items:newItemsList}
    })
  }

  createNewItem = () => {
    this.setState(({items}) => {
      let newItems = [...items]
      newItems.push({
        id: this.id,
        title: `item ${newItems.length + 1}`,
        text: `Some info ${newItems.length + 1}`,
        selected: false
      })
      this.id++
      return {items: newItems}
    })
  }

  render(){

    return(
      <div className="crud">
        <CrudHeader
          headerTitle={this.state.headerTitle}
          canCreate={this.state.canCreate}
          createNewItem={this.createNewItem}
        />
        <CrudItemList
          itemList={this.state.items}
          selectItem={this.selectItem}
          canEdit={this.state.canEdit}
          setText={this.setText}
        />
        {this.state.canDelete ?
          <CrudBottom
            deleteSelectItems={this.deleteSelectItems}
            selectUnselect={this.selectUnselect}
          />
          : false}
      </div>
    )
  }

}