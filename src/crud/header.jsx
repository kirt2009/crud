import React from 'react'

const CrudHeader = ({headerTitle,createNewItem,canCreate}) => {

  return(
    <div className="crudHeader">
      <div className="crudTitle">{headerTitle}</div>
      {canCreate ? <button className="crudButton" onClick={() => createNewItem()}>Create new</button> : false}
    </div>
  )
}

export default CrudHeader