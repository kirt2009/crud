import React from 'react'

const CrudBottom = ({deleteSelectItems,selectUnselect}) => {
  return(
    <div className="crudBottom">
      <button className="crudButton" onClick={() => selectUnselect()}>Select/Unselect all</button>
      <button className="crudButton crudButton--delete" onClick={() => deleteSelectItems()}>Delete selected</button>
    </div>
  )
}

export default CrudBottom