import React from 'react'
import CrudItem from './item'

const ItemList = ({itemList,selectItem,canEdit,setText}) => {

  const items = itemList.map(({title,text,selected,id}) => {
    return (
      <CrudItem
        title={title}
        text={text}
        selected={selected}
        key={id}
        id={id}
        selectItem={() => selectItem(id)}
        canEdit={canEdit}
        setText={setText}
      />
    )
  })

  return(
    <React.Fragment>
      {items}
    </React.Fragment>
  )
}

export default ItemList