import React,{Component} from 'react'

export default class CrudItem extends Component {

  state = {
    edit: false,
    view: false,
    text: this.props.text
  }

  setView = () => {
    this.setState(({view}) => {
      return {view: !view}
    })
  }

  setEdit = () => {
    this.setState(({edit}) => {
      return {edit: !edit,view:true}
    })
  }

  setText = (e) => {
    this.setState({text:e.target.value})
  }

  save = () => {
    let {id,setText,itemsName} = this.props
    setText(id,this.state.text,itemsName)
    this.setState({edit:false})
  }

  cancel = () => {
    this.setState({edit:false,text:this.props.text,view:false})
  }

  render(){
    const {title,text,selected,selectItem,canEdit} = this.props
    const crudCheckbox = selected ? 'crudCheckbox crudCheckbox--active' : 'crudCheckbox'
    const canEditButton = canEdit ?
      <span className="crudTextButton" onClick={this.setEdit}>{this.state.edit ? 'view' : 'edit'}</span>
      : false

    return(
      <div className="crudItem">
        <div className="crudItem__header">
          {canEdit ? <div className={crudCheckbox} onClick={selectItem} ></div> : false}
          <div className="crudItem__topBlock">
            <div className="crudItem__title" onClick={this.setView}>{title}</div>
            <div className="crudItem__buttons">
              {canEditButton}
              {(canEdit && this.state.edit) ?
                <React.Fragment>
                  <span className="crudTextButton" onClick={this.save}>save</span>
                  <span className="crudTextButton" onClick={this.cancel}>cancel</span>
                </React.Fragment>
              : false}
            </div>
          </div>
        </div>
        {this.state.view ?
          <div className="crudItem__content">
            {this.state.edit ?
              <input
                type="text"
                className="crudItem__inputText"
                placeholder="Input text"
                value={this.state.text}
                onChange={this.setText}
                /> :
              <div className="crudItem__text">{text}</div>
            }
          </div> : false
        }
      </div>
    )

  }

}